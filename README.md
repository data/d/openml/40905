# OpenML dataset: Banana

https://www.openml.org/d/40905

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;. This dataset is used in &quot;Learning hyperparameters for unsupervised anomaly detection. A. Thomas, S. Cl&eacute;men&ccedil;on, V. Feuillard, A. Gramfort. Anomaly Detection Workshop, ICML 2016.&quot;

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40905) of an [OpenML dataset](https://www.openml.org/d/40905). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40905/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40905/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40905/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

